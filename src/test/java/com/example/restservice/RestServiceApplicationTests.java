package com.example.restservice;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class RestServiceApplicationTests {

	@Test
	public void TestIfGreetingReturnsHelloWorld() {
		String expected = "Hello World!";
		HelloWorld helloWorld = new HelloWorld();

		String actual = helloWorld.greeting();

		assertEquals(expected,actual);
	}

}
