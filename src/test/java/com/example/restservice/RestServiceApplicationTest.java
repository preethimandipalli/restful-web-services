package com.example.restservice;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class RestServiceApplicationTest{

	@Test
	public void contextLoads() {
	}

	@Test
	public void testRandomNumberIsBetweenOneAndTen(){
		RandomNumber randomNumber =new RandomNumber();

		int actualNumber=randomNumber.generateRandomNumber();

		assertTrue(0<=actualNumber);
		assertTrue(actualNumber<=10);
	}

}
