package com.example.restservice;

import java.lang.Math;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class RandomNumber {

    @GetMapping("/number")
    public int generateRandomNumber() {
        return (int) (Math.random()*10);
    }
}